
from django.urls import path
from videos.views import index

urlpatterns = [
    path('', index),
]
